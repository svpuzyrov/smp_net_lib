#ifndef MESSAGE_H
#define MESSAGE_H

#include "net_types.h"
#include "net_utils.h"

namespace smp_net
{

template <typename T, int size>
union u_message
{
    T u_data;
    char buffer[size];
    u_message()
    {}
    u_message(const T& data)
        : u_data(data)
    {}
};

template <typename T>
struct simple_message
{
    u_message<T, sizeof(T)> u_msg;

    simple_message()
    {}
    simple_message(const T& msg)
        : u_msg(msg)
    {}
};

typedef simple_message<short> simple_short_message;
typedef simple_message<int> simple_int_message;
typedef simple_message<long> simple_long_message;

template <typename T>
struct message
{
public:
    friend class ip_socket;
    friend class tcp_socket;
    friend class tcp_base_socket;
    message(const T& data)
        : data_(data)
    {}
    virtual ~message()
    {}
protected:
    T data_;
    virtual char* get_buffer() const
    {
        return static_cast<char*>(&data_);
    }

    virtual char* get_buffer()
    {
        return static_cast<char*>(&data_);
    }

    virtual uint get_buffer_size() const
    {
        return sizeof(data_);
    }
};

template<>
struct message<char*>
{
    friend class ip_socket;
    friend class tcp_base_socket;
    friend class tcp_socket;
    friend class tcp_server_socket;

    message(const uint sz, char fill_char = ' ')
            : data_(new char[sz + 1])
    {
        memset(data_,fill_char,sz);
        data_[sz] = '\0';
    }
    message(const char* data)
            : data_(new char[::strlen(data) + 1])
    {
        ::strcpy(data_,data);
        data_[strlen(data)] = '\0';
    }
    virtual ~message()
    {
        delete[] data_;
    }
    char* get_data() const
    { return data_; }
    char* get_data()
    { return data_; }
protected:
    char* data_;
    virtual char* get_buffer() const
    {
        return data_;
    }
    virtual char* get_buffer()
    {
        return data_;
    }
    virtual uint get_buffer_size() const
    {
        return ::strlen(data_);
    }
};

template<>
struct message<std::string>
{
    friend class ip_socket;
    friend class tcp_socket;
    friend class tcp_base_socket;
    message(const std::string& data)
            : data_(data)
    {}
    virtual ~message()
    {}
protected:
    std::string data_;
    virtual char* get_buffer() const
    {
        return const_cast<char*>(data_.c_str());
    }
    virtual uint get_buffer_size() const
    {
        return data_.length() + 1;
    }
};

}

#endif // MESSAGE_H
