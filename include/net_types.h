#ifndef _NET_TYPES_H
#define _NET_TYPES_H

    #include <iostream>
    #include <cstdio>
    #include <cstdlib>
    #include <cstring>
    #include <string>
    #include <sstream>

typedef unsigned short ushort;
typedef unsigned int uint;
typedef unsigned long ulong;

#ifdef _WIN32 || _WIN64

    #include <windows.h>
    #include <winsock2.h>
    #include <ws2tcpip.h>

#else

    #include <sys/time.h>
    #include <sys/socket.h>
    #include <sys/types.h>
    #include <sys/select.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <netdb.h>
    #include <unistd.h>
    #include <pthread.h>
    #include <errno.h>
    #define unsigned int SOCKET;

#endif

#endif // _NET_TYPES_H
