#ifndef _IPSOCKET_H
#define _IPSOCKET_H

#include "net_types.h"
#include "net_utils.h"
#include "end_point.h"
#include "message.h"
#include <exception>

namespace smp_net
{
struct addr_in_use_exception : public std::exception
{};

struct socket_not_support_exception : public std::exception
{};

class ip_socket
{
    public:
        enum SOCKET_TYPE {
            TCP_SOCKET = SOCK_STREAM,
            UDP_SOCKET = SOCK_DGRAM,
            RAW_SOCKET = SOCK_RAW};
        enum PROTOCOL_TYPE {
            IP_PROTOCOL   = IPPROTO_IP,
            IPV6_PROTOCOL = IPPROTO_IPV6,
            ICMP_PROTOCOL = IPPROTO_ICMP,
            IGMP_PROTOCOL = IPPROTO_IGMP,
            TCP_PROTOCOL  = IPPROTO_TCP,
            UDP_PROTOCOL  = IPPROTO_UDP,
            RAW_PROTOCOL  = IPPROTO_RAW
            };
        ip_socket(SOCKET_TYPE socket_type = TCP_SOCKET
                  , PROTOCOL_TYPE proto_type = IP_PROTOCOL);

        ip_socket(const end_point& endpoint
                  , bool auto_bind
                  , SOCKET_TYPE socket_type = TCP_SOCKET
                  , PROTOCOL_TYPE proto_type = IP_PROTOCOL);

        ip_socket(const end_point& endpoint
                  , SOCKET_TYPE socket_type = TCP_SOCKET
                  , PROTOCOL_TYPE proto_type = IP_PROTOCOL);
        virtual ~ip_socket();
        bool bind();
        bool close();
        const SOCKET& get_socket_id() const
        { return socket_id_; }
        const end_point& get_endpoint() const
        { return endpoint_; }
        void set_endpoint(const end_point& endpoint)
        { endpoint_ = endpoint_; }
    protected:
        ip_socket(SOCKET socket
                  , SOCKET_TYPE socket_type = TCP_SOCKET
                  , PROTOCOL_TYPE proto_type = IP_PROTOCOL);

        ip_socket(SOCKET socket
                  , const end_point& endpoint
                  , SOCKET_TYPE socket_type = TCP_SOCKET
                  , PROTOCOL_TYPE proto_type = IP_PROTOCOL);

        void init_socket();
    private:
        SOCKET socket_id_;
        SOCKET_TYPE socket_type_;
        PROTOCOL_TYPE protocol_type_;
        end_point endpoint_;
        socket_api_manager& sock_api_mng;
        static uint count_sockets;

        ip_socket& operator = (const ip_socket& rhs);
        ip_socket(const ip_socket&);
};
}
#endif // _IPSOCKET_H
