#ifndef _TCP_SERVER_SOCKET_H
#define _TCP_SERVER_SOCKET_H

#include "ipsock.h"
#include "tcpsock.h"
#include <memory>

namespace smp_net
{

class tcp_socket;

class tcp_server_socket : public ip_socket
{
public:
    tcp_server_socket(const end_point& endpoint
                      , bool auto_bind = true);
    tcp_server_socket(int port, bool auto_bind = true);
    ~tcp_server_socket();
    bool listen(int queue_len = DEFAULT_QUEUE_LENGTH);
    bool is_listened()
    { return is_listened_;}
    std::shared_ptr<tcp_socket> accept() const;
    static uint DEFAULT_QUEUE_LENGTH;
private:
    bool is_listened_;
};

}

#endif // _TCP_SERVER_SOCKET_H
