#ifndef _TCP_SOCKET_H
#define _TCP_SOCKET_H

#include "ipsock.h"
#include "tcpserversock.h"

namespace smp_net
{
class tcp_socket : public ip_socket
{
public:
    friend class tcp_server_socket;
    tcp_socket();
    tcp_socket(const end_point& endpoint
               , bool auto_connect = true);
    ~tcp_socket();
    bool connect();
    bool connect(const end_point& endpoint);
    bool is_connected()
    { return is_connected_; }

    template <typename T>
    int send(const simple_message<T>& message)
    {
        int ret = ::send(get_socket_id(),
                         message.u_msg.buffer,
                         sizeof(T),0);
        is_connected_ = ret > 0;
        return ret;
    }
    template <typename T>
    int send(const message<T>& message)
    {
        int ret = ::send(get_socket_id(),
                         message.get_buffer(),
                         message.get_buffer_size(),0);
        is_connected_ = ret > 0;
        return ret;
    }

    int send(const message<char*>& message);
    int send(const message<std::string>& message);

    template <typename T>
    int receive(message<T>& message)
    {
        int ret = ::recv(get_socket_id(),
                         message.get_buffer(),
                         message.get_buffer_size(),0);
        is_connected_ = ret > 0;

        return ret;
    }
    template <typename T>
    int receive(simple_message<T>& message)
    {
        int ret = ::recv(get_socket_id(),
                        message.u_msg.buffer,
                        sizeof(T),0);
        is_connected_ = ret > 0;

        return ret;
    }
    int receive(message<char*>& message);
    int receive(message<std::string>& message);
protected:
    tcp_socket(SOCKET socket);
    tcp_socket(SOCKET socket
               , const end_point& endpoint);
private:
    bool is_connected_;
};
}

#endif // _TCP_SOCKET_H
