#ifndef _UDP_SOCKET_H
#define _UDP_SOCKET_H

#include "ipsock.h"

namespace smp_net
{

class udp_socket : public ip_socket
{
    public:
        udp_socket();
        virtual ~udp_socket();
    protected:
    private:
};

}

#endif // _UDP_SOCKET_H
