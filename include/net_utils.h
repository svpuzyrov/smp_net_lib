#ifndef _NET_UTILS_H
#define _NET_UTILS_H

#include "net_types.h"

namespace smp_net
{

class socket_api_manager
{
public:
    static socket_api_manager& new_instance()
    {
        static socket_api_manager sock_api_man;
        return sock_api_man; }
    virtual ~socket_api_manager();
private:
    socket_api_manager();
    socket_api_manager(const socket_api_manager&);
    socket_api_manager& operator = (const socket_api_manager&);
};

}
#endif
