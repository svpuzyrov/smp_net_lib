#ifndef END_POINT_H
#define END_POINT_H

#include "net_types.h"
#include "net_utils.h"

namespace smp_net
{

class end_point
{
    public:
        enum ENDPOINT_TYPE
        {
            ET_SERVER_TYPE = 0,
            ET_CLIENT_TYPE = 1
        };
        friend class ip_socket;
        friend class tcp_base_socket;
        friend class tcp_socket;
        friend class tcp_server_socket;
        end_point();
        end_point(ushort port);
        end_point(const char* address, ushort port);
        end_point(const std::string& address, ushort port);
        virtual ~end_point();

        ENDPOINT_TYPE get_endpoint_type() const
        { return endpoint_type; }
        short get_port()
        { return ntohs(addr.sin_port); }
    protected:
        end_point(const sockaddr_in& sock_addr);
        virtual const sockaddr_in& get_addr_in_struct() const
        { return addr; }
        virtual uint get_addr_struct_size() const
        { return sizeof(addr); }
    private:
        struct sockaddr_in addr;
        ENDPOINT_TYPE endpoint_type;
        void str_to_sock_addr(const char* addr_str, ushort port);
};

}

#endif // END_POINT_H
