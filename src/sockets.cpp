#include "sockets.h"

namespace smp_net
{

uint ip_socket::count_sockets = 0;

/*
* IP socket class
*/

ip_socket::ip_socket(const end_point& endpoint
                     , bool auto_bind
                     , SOCKET_TYPE socket_type
                     , PROTOCOL_TYPE protocol_type)
    : socket_id_(0)
    , socket_type_(socket_type)
    , protocol_type_(protocol_type)
    , endpoint_(endpoint)
    , sock_api_mng(socket_api_manager::new_instance())
{
    set_socket();
    if (auto_bind)
        bind();
    ++count_sockets;
}

ip_socket::~ip_socket()
{
    unset_socket();
    --count_sockets;
}

bool ip_socket::bind()
{
    sockaddr* addr = get_endpoint().get_addr_struct();
    int sz = get_endpoint().get_addr_struct_size();
    return ::bind(socket_id_, addr, sz) == 0;
}

void ip_socket::set_socket()
{
    socket_id_ = socket(AF_INET,socket_type_,protocol_type_);
}

void ip_socket::unset_socket()
{
    #ifdef _WIN32 || _WIN64
    closesocket(socket_id_);
    #else
    close(socket_id_);
    #endif // _WIN32
}

/*
* TCP base socket class
*/

tcp_base_socket::tcp_base_socket(const end_point& endpoint
                                 , bool auto_bind)
    : ip_socket(endpoint,auto_bind, TCP_SOCKET, TCP_PROTOCOL)
{
    if (auto_bind)
        bind();
}

tcp_base_socket::~tcp_base_socket()
{}

template <typename T>
int tcp_base_socket::send(const simple_message<T>& message, const ip_socket& endsocket)
{
    return ::send(endsocket.get_socket_id(),
                  message.get_buffer(),
                  message.get_buffer_size(),0);
}

template <typename T>
int tcp_base_socket::send(const simple_message<T>& message, const end_point& endpoint)
{
    return send(message, tcp_base_socket(endpoint));
}

template <typename T>
int tcp_base_socket::send(const message<T>& message, const ip_socket& endsocket)
{
    return ::send(endsocket.get_socket_id(),
                  message.get_buffer(),
                  message.get_buffer_size(),0);
}

template <>
int tcp_base_socket::send(const message<char*>& message, const ip_socket& endsocket)
{
    return ::send(endsocket.get_socket_id(),
                  message.get_buffer(),
                  message.get_buffer_size(),0);
}

template <>
int tcp_base_socket::send(const message<std::string>& message, const ip_socket& endsocket)
{
    return ::send(endsocket.get_socket_id(),
                  message.get_buffer(),
                  message.get_buffer_size(),0);
}

template <typename T>
int tcp_base_socket::send(const message<T>& message, const end_point& endpoint)
{
    return send(message, tcp_base_socket(endpoint));
}

template <>
int tcp_base_socket::send(const message<char*>& message, const end_point& endpoint)
{
    return send(message, tcp_base_socket(endpoint));
}

template <>
int tcp_base_socket::send(const message<std::string>& message, const end_point& endpoint)
{
    return send(message, tcp_base_socket(endpoint));
}

template <typename T>
int tcp_base_socket::receive(simple_message<T>& message)
{
    return ::recv(get_socket_id(),
                  message.get_buffer(),
                  message.get_buffer_size(),0);
}

template <typename T>
int tcp_base_socket::receive(message<T>& message)
{
    return ::recv(get_socket_id(),
                  message.get_buffer(),
                  message.get_buffer_size(),0);
}

template <>
int tcp_base_socket::receive<char*>(message<char*>& message)
{
    return ::recv(get_socket_id(),
                  message.get_buffer(),
                  message.get_buffer_size(),0);
}

template <>
int tcp_base_socket::receive<std::string>(message<std::string>& message)
{
    return ::recv(get_socket_id(),
                  message.get_buffer(),
                  message.get_buffer_size(),0);
}

/*
* TCP client socket class
*/
tcp_client_socket::tcp_client_socket(const end_point& endpoint
                                     , bool auto_bind)
                    : tcp_base_socket(endpoint,auto_bind)
                    , is_connected_(false)
{
}

tcp_client_socket::~tcp_client_socket()
{
}

bool tcp_client_socket::connect()
{

    sockaddr* addr = get_endpoint().get_addr_struct();
    int sz = get_endpoint().get_addr_struct_size();
    is_connected_ =  ::connect(get_socket_id(), addr, sz) == 0;

    return is_connected_;
}

tcp_server_socket::tcp_server_socket(const end_point& endpoint
                                     , bool auto_bind)
                    : tcp_base_socket(endpoint, auto_bind)
{
}

tcp_server_socket::tcp_server_socket(int port, bool auto_bind)
                    : tcp_base_socket(end_point(port), auto_bind)
{
}

tcp_server_socket::~tcp_server_socket()
{
}

bool tcp_server_socket::listen(int queue_len)
{
    return ::listen(get_socket_id(), queue_len) == 0;
}

tcp_client_socket* tcp_server_socket::accept() const
{
    sockaddr_in client_addr;
    int client_addr_length = sizeof(client_addr);
    if (::accept(get_socket_id(), (sockaddr*) &client_addr, &client_addr_length))
    {
        return NULL;
    }

    return new tcp_client_socket(end_point(client_addr));
}

}
