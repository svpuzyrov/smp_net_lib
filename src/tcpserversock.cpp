#include "tcpserversock.h"

namespace smp_net
{
uint tcp_server_socket::DEFAULT_QUEUE_LENGTH = 5;

tcp_server_socket::tcp_server_socket(const end_point& endpoint
                                     , bool auto_bind)
                    : ip_socket(endpoint, auto_bind,
                                TCP_SOCKET, TCP_PROTOCOL)
{}

tcp_server_socket::tcp_server_socket(int port, bool auto_bind)
                    : ip_socket(end_point(port), auto_bind,
                                TCP_SOCKET, TCP_PROTOCOL)
{}

tcp_server_socket::~tcp_server_socket()
{
}

bool tcp_server_socket::listen(int queue_len)
{
    return ::listen(get_socket_id(), queue_len) == 0;
}

std::shared_ptr<tcp_socket> tcp_server_socket::accept() const
{
    struct sockaddr_in client_addr;
    #ifdef _WIN32 || _WIN64
    int client_addr_length = sizeof(client_addr);
    #else
    uint client_addr_length = sizeof(client_addr);
    #endif
    SOCKET socket_id = ::accept(get_socket_id(), (struct sockaddr*) &client_addr, &client_addr_length);
    if (!socket_id)
    {
        return NULL;
    }

    std::shared_ptr<tcp_socket> client_socket(new tcp_socket(socket_id, end_point(client_addr)));
    return client_socket;
}

}
