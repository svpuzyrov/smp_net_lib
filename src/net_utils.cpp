#include "net_utils.h"

namespace smp_net
{

bool init_soket_api()
{
    #ifdef _WIN32 || _WIN64
    WSAData ws;
    return WSAStartup(MAKEWORD(2,2),&ws);

    #else
    return 0;
    #endif // _WIN32
}

bool deinit_soket_api()
{
    #ifdef _WIN32 || _WIN64
    return WSACleanup();
    #else
    return 0;
    #endif // _WIN32
}

socket_api_manager::socket_api_manager()
{
    init_soket_api();
}

socket_api_manager::~socket_api_manager()
{
    deinit_soket_api();
}

}
