#include "tcpsock.h"

namespace smp_net
{
/*
* TCP socket class
*/
tcp_socket::tcp_socket()
            : ip_socket(TCP_SOCKET,TCP_PROTOCOL)
            , is_connected_(false)
{}

tcp_socket::tcp_socket(SOCKET socket)
            : ip_socket(socket,TCP_SOCKET, TCP_PROTOCOL)
            , is_connected_(false)
{}

tcp_socket::tcp_socket(SOCKET socket
                       , const end_point& endpoint)
             : ip_socket(socket, endpoint, TCP_SOCKET, TCP_PROTOCOL)
             , is_connected_(false)
{
}

tcp_socket::tcp_socket(const end_point& endpoint
                       , bool auto_connect)
                    : ip_socket(endpoint, TCP_SOCKET, TCP_PROTOCOL)
                    , is_connected_(false)
{
    if (auto_connect)
        connect();
}

tcp_socket::~tcp_socket()
{
}

bool tcp_socket::connect(const end_point& endpoint)
{
    if (is_connected_)
    {
        close();
        is_connected_ = false;
    }

    init_socket();
    set_endpoint(endpoint);
    return connect();
}

bool tcp_socket::connect()
{
    if (is_connected_)
        return true;
    is_connected_ =  ::connect(get_socket_id(),
                               (sockaddr*)& (get_endpoint().get_addr_in_struct()),
                                get_endpoint().get_addr_struct_size()) == 0;

    return is_connected_;
}

int tcp_socket::send(const message<char*>& message)
{
    if (!is_connected_)
        return -1;
    int ret = ::send(get_socket_id(),
                  message.get_buffer(),
                  message.get_buffer_size(),0);
    is_connected_ = ret > 0;
    return ret;
}

int tcp_socket::send(const message<std::string>& message)
{
    if (!is_connected_)
        return -1;
    int ret = ::send(get_socket_id(),
                  message.get_buffer(),
                  message.get_buffer_size(),0);
    is_connected_ = ret > 0;
    return ret;
}

int tcp_socket::receive(message<char*>& message)
{
    if (!is_connected_)
        return -1;
    int ret = ::recv(get_socket_id(),
                  message.get_buffer(),
                  message.get_buffer_size(),0);
    is_connected_ = ret > 0;
    if (is_connected_)
    {
        message.get_buffer()[ret] = '\0';
    }

    return ret;
}

int tcp_socket::receive(message<std::string>& message)
{
    if (!is_connected_)
        return -1;
    int ret = ::recv(get_socket_id(),
                  message.get_buffer(),
                  message.get_buffer_size(),0);
    is_connected_ = ret > 0;
    if (is_connected_)
    {
        message.get_buffer()[ret] = '\0';
    }

    return ret;
}


}
