#include "ipsock.h"

namespace smp_net
{
uint ip_socket::count_sockets = 0;

/*
* IP socket class
*/

ip_socket::ip_socket(SOCKET socket
                     , SOCKET_TYPE socket_type
                     , PROTOCOL_TYPE protocol_type)
    : socket_id_(socket)
    , socket_type_(socket_type)
    , protocol_type_(protocol_type)
    , sock_api_mng(socket_api_manager::new_instance())
{
    ++count_sockets;
}

ip_socket::ip_socket(SOCKET_TYPE socket_type
                     , PROTOCOL_TYPE protocol_type)
    : socket_id_(0)
    , socket_type_(socket_type)
    , protocol_type_(protocol_type)
    , sock_api_mng(socket_api_manager::new_instance())
{
    init_socket();
    ++count_sockets;
}

ip_socket::ip_socket(SOCKET socket
                     , const end_point& endpoint
                     , SOCKET_TYPE socket_type
                     , PROTOCOL_TYPE protocol_type)
    : socket_id_(socket)
    , socket_type_(socket_type)
    , protocol_type_(protocol_type)
    , endpoint_(endpoint)
    , sock_api_mng(socket_api_manager::new_instance())
{
    ++count_sockets;
}

ip_socket::ip_socket(const end_point& endpoint
                     , bool auto_bind
                     , SOCKET_TYPE socket_type
                     , PROTOCOL_TYPE protocol_type)
    : socket_id_(0)
    , socket_type_(socket_type)
    , protocol_type_(protocol_type)
    , endpoint_(endpoint)
    , sock_api_mng(socket_api_manager::new_instance())
{
    init_socket();
    if (auto_bind)
        bind();
    ++count_sockets;
}

ip_socket::ip_socket(const end_point& endpoint
                     , SOCKET_TYPE socket_type
                     , PROTOCOL_TYPE protocol_type)
    : socket_id_(0)
    , socket_type_(socket_type)
    , protocol_type_(protocol_type)
    , endpoint_(endpoint)
    , sock_api_mng(socket_api_manager::new_instance())
{
    init_socket();
    ++count_sockets;
}

ip_socket::~ip_socket()
{
    close();
    --count_sockets;
}

bool ip_socket::bind()
{
    return ::bind(socket_id_,
                  (sockaddr*)&(endpoint_.get_addr_in_struct()),
                  endpoint_.get_addr_struct_size()) == 0;
}

void ip_socket::init_socket()
{
    socket_id_ = socket(AF_INET,socket_type_,protocol_type_);
}

bool ip_socket::close()
{
    #ifdef _WIN32 || _WIN64
    return closesocket(socket_id_) == 0;
    #else
    close(socket_id_);
    return true;
    #endif // _WIN32
}


}
