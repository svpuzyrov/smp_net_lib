#include "end_point.h"

namespace smp_net
{

end_point::end_point()
{
    memset(&addr,0,sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = 0;
    endpoint_type = ET_SERVER_TYPE;

    socket_api_manager& sap_man = socket_api_manager::new_instance();
    SOCKET s = socket(AF_INET,SOCK_STREAM,0);
    int addr_len = sizeof(addr);
    if (!bind(s,(sockaddr*)&addr,addr_len))
    {
        getsockname(s,(sockaddr*)&addr,&addr_len);
    }
    closesocket(s);
}

end_point::end_point(ushort port)
{
    memset(&addr,0,sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = ::htonl(INADDR_ANY);
    addr.sin_port = ::htons(port);
    endpoint_type = ET_SERVER_TYPE;
}

end_point::end_point(const char* address, ushort port)
{
    str_to_sock_addr(address,port);
    endpoint_type = ET_CLIENT_TYPE;
}

end_point::end_point(const std::string& address, ushort port)
{
    str_to_sock_addr(address.c_str(),port);
    endpoint_type = ET_CLIENT_TYPE;
}

end_point::end_point(const sockaddr_in& sock_addr)
            : addr(sock_addr)
{}

end_point::~end_point()
{

}

void end_point::str_to_sock_addr(const char* addr_str, ushort port)
{
    memset(&addr,0,sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = ::htons(port);
    addr.sin_addr.s_addr = inet_addr(addr_str);

    if (addr.sin_addr.s_addr == INADDR_NONE)
    {
        struct hostent* ht = gethostbyname(addr_str);
        memcpy(&addr.sin_addr.s_addr,ht->h_addr_list[0],ht->h_length);
    }

}

}
